<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        // use faker to generate fake data
        $faker = Factory::create('fr_FR');

        // create 10 Authors
        $authors = [];
        for ($i = 0; $i < 10; ++$i) {
            $author = new Author();
            $author->setName($faker->name);
            $manager->persist($author);
            $authors[] = $author;
        }

        // create 10 Publisher
        $publishers = [];
        for ($i = 0; $i < 10; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->company);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // create Status
        $status = [];
        foreach (['to-read', 'reading', 'read'] as $s) {
            $oneStatus = new Status();
            $oneStatus->setName($s);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        // create 100 Books
        $books = [];
        for ($i = 0; $i < 100; ++$i) {
            $book = new Book();

            /** @phpstan-ignore-next-line  */
            $isbn10 = $faker->isbn10;

            /** @phpstan-ignore-next-line  */
            $isbn13 = $faker->isbn13;

            $book
                ->setGoogleBooksId($faker->uuid)
                ->setTitle($faker->sentence())
                ->setSubtitle($faker->sentence())
                ->setPublishDate($faker->dateTimeBetween('-50 years'))
                ->setDescription($faker->paragraph())
                ->setIsbn10($isbn10)
                ->setIsbn13($isbn13)
                ->setPageCount($faker->numberBetween(100, 1000))
                ->setSmallThumbnail($faker->imageUrl(100, 150))
                ->setThumbnail($faker->imageUrl(200, 300))
                ->addAuthor($faker->randomElement($authors))
                ->addPublisher($faker->randomElement($publishers))
            ;
            $manager->persist($book);
            $books[] = $book;
        }

        // create 10 User
        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $user
                ->setEmail("user . $i @gmail.com")
                ->setPassword($this->passwordHasher->hashPassword($user, 'password'))
                ->setPseudo($faker->userName)
                ->setIsVerified($faker->boolean)
                ->setRoles(['ROLE_USER'])
            ;
            $manager->persist($user);
            $users[] = $user;
        }

        // create 10 UserBook (with random status) by User
        foreach ($users as $user) {
            for ($i = 0; $i < 10; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setUser($user)
                    ->setBook($faker->randomElement($books))
                    ->setStatus($faker->randomElement($status))
                    ->setComment($faker->paragraph())
                    ->setRating($faker->numberBetween(0, 5))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ;
                $manager->persist($userBook);
            }
        }

        $manager->flush();
    }
}
