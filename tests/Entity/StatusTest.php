<?php

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    //test getter and setter
    public function testGetId()
    {
        $status = new Status();

        $status->setName('test');
        $this->assertEquals('test', $status->getName());

        //test id
        $this->assertNull($status->getId());

    }

    //test Add UserBook
    public function testAddAndRemoveUserBook()
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $this->assertTrue($status->getUserBooks()->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());

    }

    //test Remove UserBook
    public function testRemoveUserBook()
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $status->removeUserBook($userBook);
        $this->assertFalse($status->getUserBooks()->contains($userBook));
        $this->assertNull($userBook->getStatus());
    }
}
