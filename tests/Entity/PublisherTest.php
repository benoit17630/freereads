<?php

namespace App\Tests\Entity;

use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    //test getter and setter
    public function testGetAndSet()
    {
        $publisher = new Publisher();
        $publisher->setName('test');
        $this->assertEquals('test', $publisher->getName());

        //test id
        $this->assertNull($publisher->getId());
    }

    //test Add and Remove Book
    public function testAddAndRemoveBook()
    {
        $publisher = new Publisher();
        $book = $this->createMock('App\Entity\Book');
        $publisher->addBook($book);
        $this->assertEquals($book, $publisher->getBooks()[0]);
        $publisher->removeBook($book);
        $this->assertEmpty($publisher->getBooks());
    }
}
