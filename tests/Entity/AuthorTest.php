<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    //test getId
    public function testGetId()
    {
        $author = new Author();
        $this->assertEquals(null, $author->getId());
    }

    //test name
    public function testName()
    {
        $author = new Author();
        $author->setName('name');
        $this->assertEquals('name', $author->getName());
    }

    //test books
    public function testBooks()
    {
        $author = new Author();
        $book1 = new Book();
        $book2 = new Book();
        $author->addBook($book1);
        $this->assertTrue($author->getBooks()->contains($book1));

        $author->addBook($book2);
        $this->assertTrue($author->getBooks()->contains($book2));

        $author->removeBook($book1);

    }

}
