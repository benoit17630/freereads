<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    //test getId
    public function testGetId()
    {
        $user = new User();
        $this->assertEquals(null, $user->getId());
    }

    //test email
    public function testEmail()
    {
        $user = new User();
        $user->setEmail('email');
        $this->assertEquals('email', $user->getEmail());
    }

    //test getUserIdentifier
    public function testUserIdentifier()
    {
        $user = new User();
        $user->setEmail('userIdentifier');
        $this->assertEquals('userIdentifier', $user->getUserIdentifier());
    }

    //test roles
    public function testRoles()
    {
        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
    }

    //test password
    public function testPassword()
    {
        $user = new User();
        $user->setPassword('password');
        $this->assertEquals('password', $user->getPassword());
    }

    //test eraseCredentials
    public function testEraseCredentials()
    {
        $user = new User();
        $this->assertEquals(null, $user->eraseCredentials());
    }

    //test pseudo
    public function testPseudo()
    {
        $user = new User();
        $user->setPseudo('pseudo');
        $this->assertEquals('pseudo', $user->getPseudo());
    }

    //test addUserBook
    public function testAddUserBook()
    {
        $user = new User();
        $usersBook = new UserBook();
        $user->addUserBook($usersBook);
        $this->assertEquals($usersBook, $user->getUserBooks()[0]);
    }

    //test removeUsersBook
    public function testRemoveUserBook()
    {
        $user = new User();
        $usersBook = new UserBook();
        $user->addUserBook($usersBook);
        $user->removeUserBook($usersBook);
        $this->assertEquals([], $user->getUserBooks()->toArray());
    }

    //test getUserBooks
    public function testGetUserBooks()
    {
        $user = new User();
        $usersBook = new UserBook();
        $user->addUserBook($usersBook);
        $this->assertEquals([$usersBook], $user->getUserBooks()->toArray());
    }

}
