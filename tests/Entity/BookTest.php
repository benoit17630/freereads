<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
   //test getId
    public function testGetId()
    {
        $book = new Book();
        $this->assertEquals(null, $book->getId());
    }

    //test googleBooksId
    public function testGoogleBooksId()
    {
        $book = new Book();
        $book->setGoogleBooksId('googleBooksId');
        $this->assertEquals('googleBooksId', $book->getGoogleBooksId());
    }

    //test title
    public function testTitle()
    {
        $book = new Book();
        $book->setTitle('title');
        $this->assertEquals('title', $book->getTitle());
    }

    //test subtitle
    public function testSubtitle()
    {
        $book = new Book();
        $book->setSubtitle('subtitle');
        $this->assertEquals('subtitle', $book->getSubtitle());
    }

    //test publishDate
    public function testPublishDate()
    {
        $book = new Book();
        $date = new \DateTime();
        $book->setPublishDate($date);
        $this->assertEquals($date, $book->getPublishDate());
    }

    //test description
    public function testDescription()
    {
        $book = new Book();
        $book->setDescription('description');
        $this->assertEquals('description', $book->getDescription());
    }

    //test isbn10
    public function testIsbn10()
    {
        $book = new Book();
        $book->setIsbn10('isbn10');
        $this->assertEquals('isbn10', $book->getIsbn10());
    }

    //test isbn13
    public function testIsbn13()
    {
        $book = new Book();
        $book->setIsbn13('isbn13');
        $this->assertEquals('isbn13', $book->getIsbn13());
    }

    //test pageCount
    public function testPageCount()
    {
        $book = new Book();
        $book->setPageCount(1);
        $this->assertEquals(1, $book->getPageCount());
    }

    //test smallThumbnail
    public function testSmallThumbnail()
    {
        $book = new Book();
        $book->setSmallThumbnail('smallThumbnail');
        $this->assertEquals('smallThumbnail', $book->getSmallThumbnail());
    }

    //test thumbnail
    public function testThumbnail()
    {
        $book = new Book();
        $book->setThumbnail('thumbnail');
        $this->assertEquals('thumbnail', $book->getThumbnail());
    }

    //test add and remove author
    public function testAddAndRemoveAuthor()
    {
        $book = new Book();
        $author1 = new Author();
        $author2 = new Author();

        $book->addAuthor($author1);
        $book->addAuthor($author2);

        $this->assertTrue($book->getAuthors()->contains($author1));
        $this->assertTrue($book->getAuthors()->contains($author2));

        $book->removeAuthor($author1);
        $this->assertFalse($book->getAuthors()->contains($author1));

        $book->removeAuthor($author2);
        $this->assertFalse($book->getAuthors()->contains($author2));
    }

    //test add and remove publisher
    public function testAddAndRemovePublisher()
    {
        $book = new Book();
        $publisher1 = new Publisher();
        $publisher2 = new Publisher();

        $book->addPublisher($publisher1);
        $book->addPublisher($publisher2);

        $this->assertTrue($book->getPublishers()->contains($publisher1));
        $this->assertTrue($book->getPublishers()->contains($publisher2));

        $book->removePublisher($publisher1);
        $this->assertFalse($book->getPublishers()->contains($publisher1));

        $book->removePublisher($publisher2);
        $this->assertFalse($book->getPublishers()->contains($publisher2));
    }

    //test add and remove userBook
    public function testAddAndRemoveUserBook()
    {
        $book = new Book();
        $userBook1 = new UserBook();
        $userBook2 = new UserBook();

        $book->addUserBook($userBook1);
        $book->addUserBook($userBook2);

        $this->assertTrue($book->getUserBooks()->contains($userBook1));
        $this->assertTrue($book->getUserBooks()->contains($userBook2));

        $book->removeUserBook($userBook1);
        $this->assertFalse($book->getUserBooks()->contains($userBook1));

        $book->removeUserBook($userBook2);
        $this->assertFalse($book->getUserBooks()->contains($userBook2));
    }


}
