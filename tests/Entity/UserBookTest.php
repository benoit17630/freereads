<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    //test getId
    public function testGetId()
    {
        $userBook = new UserBook();
        $this->assertNull($userBook->getId());
    }

    //test get and set createdAt
    public function testGetAndSetCreatedAt()
    {
        $userBook = new UserBook();
        $userBook->setCreatedAt(new \DateTimeImmutable());
        $this->assertInstanceOf(\DateTimeImmutable::class, $userBook->getCreatedAt());
    }

    //test get and set updatedAt
    public function testGetAndSetUpdatedAt()
    {
        $userBook = new UserBook();
        $userBook->setUpdatedAt(new \DateTimeImmutable());
        $this->assertInstanceOf(\DateTimeImmutable::class, $userBook->getUpdatedAt());
    }

    //test get and set comment
    public function testGetAndSetComment()
    {
        $userBook = new UserBook();
        $userBook->setComment('test');
        $this->assertEquals('test', $userBook->getComment());
    }

    //test get and set rating
    public function testGetAndSetRating()
    {
        $userBook = new UserBook();
        $userBook->setRating(5);
        $this->assertEquals(5, $userBook->getRating());
    }


}
